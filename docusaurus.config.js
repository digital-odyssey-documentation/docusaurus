// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Documentation - Digital Odyssey',
  tagline: 'Partage de connaissances',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://digital-odyssey-documentation.gitlab.io/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/docusaurus/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'Digital Odyssey Documentation', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // url: 'https://yoandev.co.gitlab.io',
  // baseUrl: '/yoandev-doc-prepa/',
  // onBrokenLinks: 'throw',
  // onBrokenMarkdownLinks: 'warn',
  // favicon: 'img/favicon.ico',
  // organizationName: 'yoandev', // Usually your GitHub org/user name.
  // projectName: 'yoandev-doc-prepa', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/digital-odyssey-documentation/docusaurus/-/edit/main/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/digital-odyssey-documentation/docusaurus/-/edit/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  plugins: [
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
        language: "fr",
      }
    ],
  ],


  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Home',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo_hesso.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'Boîte à outils',
            position: 'left',
            label: 'Documentation',
          },
          {
            href: 'https://github.com/facebook/docusaurus',
            label: 'GitHub',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutoriel',
                to: '/docs/Boîte à outils',
              },
            ],
          },
          {
            title: 'Communauté',
            items: [
              {
                label: 'HES-SO Sierre',
                href: 'https://www.hevs.ch/fr',
              },
              {
                label: 'Digital Team Academy',
                href: 'https://www.digitalteamacademy.ch/',
              },
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/digital_teamacademy/',
              },
            ],
          },
          {
            title: 'Plus',
            items: [
              {
                label: 'GitLab',
                href: 'https://github.com/facebook/docusaurus',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Digital Team Academy - HES SO Sierre.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
