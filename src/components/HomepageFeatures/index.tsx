import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

type FeatureItem = {
  title: string;
  // Svg: React.ComponentType<React.ComponentProps<'svg'>>;
  imgScr: string;
};

const FeatureList: FeatureItem[] = [
  {
    title: "Digital Team Academy",
    // Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    imgScr: "/docusaurus/img/dta-top.jpg",
  },
];

function Feature({ title, imgScr }: FeatureItem) {
  return (
    <div className="text--center image-team">
      {/* <Svg className={styles.featureSvg} role="img" /> */}
      <img src={imgScr} alt={title} />
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      {FeatureList.map((props, idx) => (
        <Feature key={idx} {...props} />
      ))}
    </section>
  );
}
