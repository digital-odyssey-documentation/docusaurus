---
sidebar_position: 1
---

## Comment créer un projet 

- Sous le réperoir `docs` créer un dossier avanc le nom du projet. Ex: Veetamine
- Dans le dossier du nom de projet créer un fichier json avec le nom suivant `_category_.json`
- Compléter selon l'exemple ci-dessous

````js
{
  "label": "CIMO",
  "position": 3,
  "link": {
    "type": "generated-index",
    "description": "Projet de Business Intelligence"
  }
}
````

## Comment créer des sous-ensembles dans un projet 

- Aller dans le dossier du projet
- Créer un ficher markdown `.md`, utiliser un nom de fichier communiquant

## Utilisation du Markdown
***

  # Ceci est un H1

  ## Ceci est un H2

  ### Ceci est un H3

  **Texte en gras**

  *Texte en italique*

  Ceci est une liste :

  * Lorem
  * ipsum

  > Ceci est un citation

  :::danger Ceci est le titre du bloc !
  Ceci est un blog de **danger**
  :::

  :::success
  Ceci est un blog de **success**
  :::

## Déploiement continue




